// Write CPP code here
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/types.h>  /* for Socket data types */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <netinet/in.h> /* for IP Socket data types */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <vector>
#include <iostream>
#include <bits/stdc++.h>
#include <math.h>
#include "blowfish.h"
#define MAX 80
#define PORT 9105
#define BITS 32UL
#define SA struct sockaddr

using namespace std;

void func(int sockfd)
{
    char buffer[65550];
    int newData;
    newData = recv(sockfd, buffer, 65550, 0);

    BLOWFISH bf("FEDCBA9876543210");
    string teststr = buffer;
    cout << "Received Encrypted: " << teststr << endl;
    teststr = bf.Decrypt_CBC(teststr);
    cout << "Received Plaintext: " << teststr << endl;
}

int main()
{
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    cout << "Enter a number to specify the server want to connect to: " << endl;
    cout << "0 - thing0" << endl;
    cout << "1 - thing1" << endl;
    cout << "2 - thing2" << endl;
    cout << "3 - thing3" << endl;
    cout << "4 - localhost" << endl;

    string choice;
    string ip;
    cin >> choice;
    if (choice == "0")
    {
        ip = "10.35.195.46";
    }
    else if (choice == "1")
    {
        ip = "10.35.195.47";
    }
    else if (choice == "2")
    {
        ip = "10.35.195.48";
    }
    else if (choice == "3")
    {
        ip = "10.35.195.49";
    }
    else if (choice == "4")
    {
        ip = "127.0.0.1";
    }
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip.c_str());
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");

    // function for chat
    func(sockfd);

    // close the socket
    close(sockfd);
}
